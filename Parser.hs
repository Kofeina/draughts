import System.Environment
import Control.Monad.Trans.State.Lazy
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Number
import Text.ParserCombinators.Parsec.Error
import System.IO
import Data.Maybe
import Control.Monad.IO.Class

import Draughts
import Moves

--- TODO przetestowac
-- uproszczony "parser" PDN
-- ruch Int-Int
-- bicie [Intx]Int

data PDN =   Move (Int,Int) -- pozycja startowa i koncowa
           | Kill [Int]  -- pozycja startowa to glowa, pozniej kolejne pozycje
           deriving (Show,Eq)

fromIntToPos :: Int -> Pos
fromIntToPos a
    | odd row = (row, col)
    | otherwise = (row, col - 1)
    where row = ((a -1) `div` 4 ) + 1
          col = (((a -1) `mod` 4) + 1) *2

pdnToMove :: PDN -> Moves.Move
pdnToMove (Move (from,to)) = Step (fromIntToPos from)  (fromIntToPos to) 
pdnToMove (Kill (from:to:_)) = Capture (fromIntToPos from) (fromIntToPos to) 


-- instance Show PDN where
--   show (Move (a,c)) = (show a)++ "-"++ (show c)
--   show (Kill [p]) = (show p)
--   show (Kill (p:ps)) = (show p)++ "x" ++ show (Kill ps)


parsePos :: Parser Int
parsePos = do
            x <- int
            if (x<1 || x>32) then
              unexpected "Tylko liczby od 1-32"
            else
              return x

parseMove = do
            x1 <- parsePos
            (char '-')
            x2 <- parsePos
            eof
            return $ Move (x1,x2)

parseKill = do
            x1 <- sepBy (parsePos) (char 'x')
            eof
            if (length x1) > 1 then
              return $ Kill x1
            else
              unexpected "start i koniec minimum"

parsePDN =  try parseMove <|> parseKill

type Game a = IO a

play :: String -> Board -> Game Board
play i brd = do
  mv <- case parse parsePDN "sPDN err" i of
    Right move -> return move
    Left x -> fail $ show x
    
  brd2 <- return $ makeMove brd (pdnToMove mv)
  putStrLn $ showBoard brd2
  hFlush stdout
  
  return brd2
  
  


doPlay :: Board -> Game ()
doPlay brd = do
    line <- getLine
    brd2 <- play line brd
    doPlay brd2
    return ()


main :: IO ()
main = do
  args <- getArgs
  progName <- getProgName
  mapM_ putStrLn args
  putStrLn progName
  putStrLn $ showBoard initBoard
  hFlush stdout
  doPlay initBoard
