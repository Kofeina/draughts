module Draughts where

import Data.Maybe (fromJust)
import Data.List (elemIndex)
import Data.List (findIndices)

data Piece = Piece Color Figure deriving (Show, Eq)
data Figure = Queen | Normal deriving (Show, Eq)
data Color = White | Black deriving (Show, Eq)
--data Pos = Pos Int

type PosPiece = (Pos, Piece)

pieceColor :: Piece -> Color
pieceColor (Piece color _) = color

type Board = [[Square]]
type Square = Maybe Piece
type Pos = (Int, Int)

squareColor :: Square -> Maybe Color
squareColor = fmap pieceColor

stringBoard = [ " b b b b",
                "b b b b ",
                " b b b b",
                "        ",
                "        ",
                "w w w w ",
                " w w w w",
                "W w w w " ]

initBoard = readBoard stringBoard

testBoard = readBoard stringBoard
testRow = head . readBoard $ stringBoard

readRow :: String -> [Square]
readRow = map readSquare

readBoard :: [String] -> Board
readBoard = map readRow

-- printBoard :: Board -> String
showSquare :: Square -> Char
showSquare Nothing       = '.'
showSquare (Just piece)  = showPiece piece

readSquare :: Char -> Square
readSquare ' ' = Nothing
readSquare c = fmap Just readPiece c

--showing and reading character
showPiece :: Piece -> Char
showPiece (Piece White Queen)   = 'W'
showPiece (Piece White Normal)  = 'w'
showPiece (Piece Black Queen)   = 'B'
showPiece (Piece Black Normal)  = 'b'

readPiece :: Char -> Piece
readPiece 'W' = (Piece White Queen)
readPiece 'w' = (Piece White Normal)
readPiece 'B' = (Piece Black Queen)
readPiece 'b' = (Piece Black Normal)

showBoard::Board->String
showBoard b = unlines $ map showRow b

showRow::[Square]->String
showRow = map showSquare

setPiece :: Pos -> Square -> Board -> Board
setPiece pos piece board = map (setRow pos piece) $ zip [1..] board  


setRow :: Pos -> Square -> (Int, [Square]) -> [Square]
setRow (row,col) newSquare (actualRow, oldRow) 
    | row == actualRow = map (setCol col newSquare) $ zip [1..] oldRow
    | otherwise = oldRow

setCol :: Int -> Square -> (Int, Square) -> Square
setCol col newSquare (actualCol, oldSquare) 
    | col == actualCol = newSquare
    | otherwise = oldSquare

getPiece :: Board -> Pos -> Maybe Piece
getPiece board (row, col) = board !! (row - 1) !! (col - 1)

getRow :: Board -> Int -> [Square]
-- getRow board row = board !! row
getRow board row = board !! (row - 1)

findColorPieces :: Board -> Color -> [(Int,Int)]
findColorPieces board color = 
        [(a+1,b+1) | a <- [0..7], b <- findInRow board color a]
    where findInRow board color p = findIndices (\x -> ( x == (Just (Piece color Queen) ) || x == (Just (Piece color Normal) ) ) )$ board!!p

move::Board->Pos->Pos->Maybe Board
move board startPos endPos = do
  startPiece <- getPiece board startPos
  movedBoard <- Just $ setSquare board endPos (Just startPiece)
  return $ setSquare movedBoard startPos Nothing

-- replaceRow b 4 (Just (Piece White Normal))

-- replaceN pozycjaZmiany nowePole (jakasPos, jakiesSquare) -> nowe
replaceN :: Int -> Square -> (Int, Square) -> (Int, Square)
replaceN changePos newSquare (pos, square)
  | changePos == pos = (pos, newSquare)
  | otherwise        = (pos, square)

replaceSquare :: [Square] -> Int -> Square -> [Square]
replaceSquare row column newSquare =
  snd . unzip $ replacedTuples
  where zippedSquares = zip [0..] row
        replacedTuples = map (replaceN column newSquare) zippedSquares

replaceRow :: Int -> [Square] -> (Int, [Square]) -> (Int, [Square])
replaceRow changeRow newSquares (row, squares)
  | changeRow == row = (row, newSquares)
  | otherwise        = (row, squares)

setSquare :: Board -> (Int, Int) -> Square -> Board
setSquare board (row, col) newSquare =
  snd . unzip $ map (replaceRow (row - 1) newRow) zippedRows
  where
    oldRow = getRow board row
    newRow = replaceSquare oldRow (col - 1) newSquare
    zippedRows = zip [0..] board
