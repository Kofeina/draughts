module Moves where
import Draughts

import Data.Maybe (fromJust, isNothing, mapMaybe)

data Move = Step Pos Pos | Capture Pos Pos deriving Show

generateAllPos :: Pos -> [[Pos]]
generateAllPos (row, col) =
  [[(row - x, col - x) | x <- [1..8]],
   [(row - x, col + x) | x <- [1..8]],
   [(row + x, col + x) | x <- [1..8]],
   [(row + x, col - x) | x <- [1..8]]]

isValid :: Pos -> Bool
isValid (row, col)
  | row < 1 || row > 8 = False
  | col < 1 || col > 8 = False
  | otherwise          = True

filterValid :: [[Pos]] -> [[Pos]]
filterValid = map . filter $ isValid

boardMoves :: Pos -> [[Pos]]
boardMoves = filterValid . generateAllPos

filterRadius :: Int -> [[Pos]] -> [[Pos]]
filterRadius = map . take

filterForward :: Color -> [[Pos]] -> [[Pos]]
filterForward Black = drop 2
filterForward White = take 2

-- bardziej nam potrzeba free, jak occupied
-- bo filter ma zostawić ma nam wolne pola (na które można wejść)
free :: Board -> Pos -> Bool
free board = isNothing . getPiece board

attachPiece :: Board -> Pos -> Maybe PosPiece
attachPiece board pos = fmap zipPosPiece $ getPiece board pos
  where zipPosPiece piece = (pos, piece)

occupiedPos :: Board -> [Pos] -> [(Pos, Piece)]
occupiedPos board = mapMaybe (attachPiece board)

firstOccupied :: Board -> [Pos] -> Maybe PosPiece
firstOccupied board posList =
  if null occupied then
    Nothing else matters -- hehe
  where matters = Just $ head occupied
        occupied = occupiedPos board posList

posHarvester :: Pos -> [Pos] -> [Pos]
posHarvester pos = takeWhile (/= pos)

blockedPos :: Board -> [Pos] -> [Pos]
blockedPos board posList =
  maybe posList harvesterUntil closestPos
  where harvesterUntil (pos, _) = posHarvester pos posList
        closestPos = firstOccupied board posList
        
-- dla figury na pozycji zwraca możliwe lądowania
normalStep :: Board -> (Pos, Piece) -> [Pos]
normalStep board (pos, (Piece _ Queen)) =
  concat $ fmap (blockedPos board) $ boardMoves pos

normalStep board (pos, (Piece color Normal)) =
  filter (free board) (concat $ destPos)
  where destPos = (filterRadius 1) . (filterForward color) . boardMoves $ pos

--isEnemy? 
--wez glowe sprawdz czy pionek przeciwny i sprawdz czy pole za jets wolne kurwa

  
enemy :: Color -> Color
enemy color = case color of 
             White -> Black
             Black -> White
  
isEnemy :: Piece -> Piece -> Bool
isEnemy (Piece color _ ) (Piece secondColor _)
    | enemy color == secondColor = True
    | otherwise = False


-- dla figury zwraca możliwe bicia
captureStep :: Board -> (Pos, Piece) -> [Pos]
captureStep board (pos, (Piece color Queen)) = 
    []
captureStep board pp@(pos, (Piece color Normal)) =
    map (!!1) $ filter (isCapture board pp) $ (filterRadius 2) . boardMoves $ pos  
    --where directions = 

    
-- sprawdzamy czy bicie w tym kierunku jest mozliwe
-- isCapture :: posPionka -> 
isCapture :: Board -> (Pos, Piece) -> [Pos] -> Bool
isCapture board (_, (Piece color Queen)) list = False
isCapture board (_, (Piece color Normal)) list
  | length list < 2 = False
  | otherwise       = free board (list !! 1) && landIsEnemy
  where piece = (Piece color Normal)
        landIsEnemy = maybe False (isEnemy piece) (getPiece board (head list))


-- dla pozycji zwraca lądowania
stepsForPos :: Board -> Pos -> [Pos]
stepsForPos board pos =
  maybe [] ((normalStep board) . zipPosPiece) (getPiece board pos)
  where zipPosPiece piece = (pos, piece)
        
-- dla pozycji zwraca bicia
capturesForPos :: Board -> Pos -> [Pos]
capturesForPos board pos =
    maybe [] ((captureStep board) . zipPosPiece) (getPiece board pos)
    where zipPosPiece piece = (pos, piece)

    
genSteps :: Board -> Pos -> [Move]
genSteps board pos = map createStep moves
    where createStep to = Step pos to 
          moves = stepsForPos board pos

genCaptures :: Board -> Pos -> [Move]
genCaptures board pos = map createCapture moves
    where createCapture to = Capture pos to
          moves = capturesForPos board pos

genMoves :: Board -> Pos -> [Move]
genMoves board pos
    | not $ null captures = captures
    | otherwise = genSteps board pos
    where captures = genCaptures board pos
          
genAllMoves :: Board -> Color -> [Move]
genAllMoves board color = concatMap (genMoves board) $ findColorPieces board color
          
--setPiece :: Pos -> Square -> Board -> Board

makeMove :: Board -> Move -> Board
makeMove board (Step from to) = 
    setPiece from Nothing $ setPiece to piece board 
    where piece = getPiece board from
makeMove board (Capture from to) =
    setPiece from Nothing $ setPiece to piece $ setPiece toCapture Nothing board
    where toCapture = ( (fst from + fst to) `div` 2, (snd from + snd to) `div` 2  )
          piece = getPiece board from
