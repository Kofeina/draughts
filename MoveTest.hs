import Draughts
import Moves

exStepBoard1 = readBoard [ " b b b b",
                           "b b b b ",
                           " b b b b",
                           "        ",
                           "   B    ",
                           "w w   w ",
                           " w w   w",
                           "W w w w " ]

exStepBoard2 = readBoard [ "  b b   ",
                           "   w b  ",
                           "  b b   ",
                           "   w w  ",
                           "        ",
                           "        ",
                           "        ",
                           "        " ]


exStep1 = stepsForPos exStepBoard1 (6, 3)
exStep2 = stepsForPos exStepBoard1 (6, 5)
exStep3 = stepsForPos exStepBoard1 (5, 4)
exStep4 = stepsForPos exStepBoard1 (3, 4)
